 # ESTUDIO SOBRE KQI NR CS HANDOVER FR% Y SUS COMPONENTES

# Chunk  not to print code lines when transforming notebook into HTML file

#  %%html
# <script>
#   function code_toggle() {
#     if (code_shown){
#       $('div.input').hide('500');
#       $('#toggleButton').val('Show Code')
#     } else {
#       $('div.input').show('500');
#       $('#toggleButton').val('Hide Code')
#     }
#     code_shown = !code_shown
#   }

#   $( document ).ready(function(){
#     code_shown=false;
#     $('div.input').hide()
#   });
# </script>
# <form action="javascript:code_toggle()"><input type="submit" id="toggleButton" value="Show Code"></form>

# ESTUDIO SOBRE KQI NR CS Handover FR% y sus componentes
# El objetivo de este estudio será entender como se distribuyen un conjunto de variables, especificadas más adelante, y su efecto sobre las quejas generadas por los clientes. Las variables seleccionadas para este estudio son:

# Handover attempts
# Act_pdp_attempts
# Four_g_attach_attempts
# nroaming_traffic
# consumption_mb
# A su vez, el alcance de la horquilla temporal considerada para este estudio es semanal, puesto que éste es el horizonte de tiempo que tiene la reasignación de las celdas más importantes por cliente. Con esto, las tres primeras variables mencionadas anteriormente, se han estudiado a nivel unitario de indicador, mientras que las dos últimas se han agrupado a nivel de quintil. La justificación de este criterio viene dada por la distribución de valores existentes en las diferentes variables analizadas, pudiendo así realizar en una correcta visualización e interpretación de los datos. Con esto, tendríamos dos tipologías de gráficos: por un lado aquellos que muestran el número de casos generados por el indicador, y por otro la agrupación de los valores relativos a horquillas de 20%. Al mismo tiempo, dada la pequeña dimensión de valores de los indicadores Handover_attempts, Act_pdp_attempts y Four_g_attach_attempts, se ha estableciido un límite de valor, a partir del cual agrupar todas las quejas, con el fin de optimizar la visualización de los resultados generados.

# Adicionalmente, se han superpuesto los ratios de queja de clientes para cada uno de los valores graficados, con el objetivo de encontrar patrones de comportamiento que justifiquen que, a mayor número de registro de casos del indicador estudiado, mayor número de clientes generarán queja. Estos ratios se han calculado por cada 10000 clientes de la población. Todas las variables vienen representadas por un conjunto de cinco gráficas. Cada una de estas gráficas contrasta la población existente en cada uno de los grupos de clasificación contra la tipología de la queja analizada, las cuales puedes ser: totales, cobertura, voz, datos u otros.

# Al mismo tiempo, cabe resaltar que, para la variable nroaming_traffic, se agrupan utilizando la media de valores semanales por imsi, mientras que para el resto de variables, la métrica utilizada de agrupación es la mediana.

%matplotlib inline
import pandas as pd
import numpy as np 
import os
import matplotlib.pyplot as plt
from datetime import datetime, date, timedelta
import logging
import sys
import smtplib
from email.utils import formatdate
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from interfaces.hdfs import HdfsInterface
from interfaces.sftp import SFTPInterface
from interfaces.impala import ImpalaInterface
from interfaces.db import DBInterface
import re
import paramiko 
import unicodedata
import operator
import subprocess
import time
import matplotlib.pyplot as plt; plt.rcdefaults()
import matplotlib.pyplot as plt
from dateutil.relativedelta import relativedelta, SU
from IPython.display import display, Markdown, Latex

import sys
!conda install --yes --prefix {sys.prefix} plotly

# Standard plotly imports
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import iplot, init_notebook_mode
# Using plotly + cufflinks in offline mode
import cufflinks
cufflinks.go_offline(connected=True)
init_notebook_mode(connected=True)

logger = logging.getLogger(__name__)

#SFTP
SFTP_HOST = '10.100.6.38'
SFTP_PORT = 22
SFTP_USERNAME = 'kernel'
SFTP_PASSWORD = 'Masmovil00*'    
SFTP_ACS_PATH = '/Gestor_ACS/'
SFTP_ACTIVATIONS_PATH = '/DWH/'
SFTP_OUTPUT_PATH = '/Kernel/'

#HDFS
HADOOP_USER_NAME = 'cem'
HADOOP_CONF_DIR = '/churn/code/cem_fija'
HADOOP_HOME = '/churn/hadoop/hadoop-2.6.0'
HADOOP_OPTS = '$HADOOP_OPTS -Djava.library.path=$HADOOP_HOME/lib/'
HADOOP_CONF_PROJECT_DIC = '/churn/code/cem_fija/.hdfscli.cfg'
HDFS_PARENT_FOLDER = f'/user/{HADOOP_USER_NAME}'
HDFS_ACS_ADSL_FOLDER = os.path.join(HDFS_PARENT_FOLDER, 'cem_fija_adsl_quality')
HDFS_ACS_FTTH_FOLDER = os.path.join(HDFS_PARENT_FOLDER, 'cem_fija_ftth_quality')
HDFS_INSTABILITY_FOLDER = os.path.join(HDFS_PARENT_FOLDER, 'cem_fija_instability')

#IMPALA
IMPALA_HOST = "impala.yoigo.inet"
IMPALA_PORT = 21050
IMPALA_DB = "cem"
IMPALA_TIMEOUT = 100
IMPALA_USER = "cem"
IMPALA_PASSWORD = "c3m999"
IMPALA_AUTH_MECHANISM = "PLAIN"

#ORACLE 
ORACLE_USER = 'kernel'
ORACLE_PASSWORD = 'kernel2014'
ORACLE_SID = 'dwyoigo'
ORACLE_HOST = '172.30.180.8'
ORACLE_ENGINE = 'oracle'

def get_impala_int():
    """
    Initialize and return a configured Yoigo Impala interface.
    :return impala_int: ImpalaInterface object
    """
    logger.info("Retrieving custom Yoigo Impala interface...")
    impala_int = ImpalaInterface(user=IMPALA_USER,
                                 password=IMPALA_PASSWORD,
                                 db=IMPALA_DB,
                                 timeout=IMPALA_TIMEOUT)
    return impala_int

def get_oracle_int():
    """
    Initialize and return a configured Yoigo ORACLE interface.
    :return oracle_int: DBInterface object
    """
    logger.info("Retrieving custom Yoigo ORACLE interface...")
    oracle_conn_str = ORACLE_ENGINE + '://' + ORACLE_USER + ':' + ORACLE_PASSWORD + '@' + ORACLE_HOST + '/' + ORACLE_SID
    oracle_int = DBInterface(oracle_conn_str)
    return oracle_int

variables = {'kqi_9' : 'zeroifnull(kqi_9) + zeroifnull(kqi_10)',
             'kqi_14' : 'kqi_14',
             'kqi_16' : 'kqi_16',
             'nroaming_traffic' : 'nroaming_traffic',
             'consumption_mb' : 'consumption_mb'}

columnas =   {'kqi_9' : 'ho_attempts',
            'kqi_14' : 'act_pdp_attempts',
            'kqi_16' : 'four_g_attach_attempts',
            'nroaming_traffic' : 'nroaming_traffic',
            'consumption_mb' : 'consumption_mb'           
           }                

map_dict = {'kqi_9' : 'kqi_9 + kqi_10',
            'kqi_14' : 'kqi_14',
            'kqi_16' : 'kqi_16',
            'nroaming_traffic' : 'nroaming_traffic',
            'consumption_mb' : 'consumption_mb'
           }

table = {'kqi_9' : 'user_qos_part_2',
'kqi_14' : 'user_qos_part_2',
'kqi_16' : 'user_qos_part_2',
'nroaming_traffic' : 'user_qos_derived_part',
'consumption_mb' : 'user_qos_derived_part'}


map_dict_2 = {'nroaming_traffic' : 'nroaming_traffic',
            'consumption_mb' : 'consumption_mb'    
}

threshold = 2

# Se definen las fechas entre las cuales se va a realizar el estudio. El alcance de la horquilla temporal es de una semana. 
# Especial atención  a aquellas semanas que incluyen dos meses diferentes, haría falta una query que recogiera de manera óptima este caso.

# Definition of dates when the analysis will take place (one week long)
# Special attention to those dates including two different months. It would be necessary to create a process able to optimize that case.

last_date = date.today() + relativedelta(weekday=SU(-3)) # Número de semanas atrás hasta el día escogido (SU: sunday)
first_date = last_date - timedelta(days = 6) # Número de días desde el último domingo considerado

impala_int = get_impala_int()
for v in variables:
    display(Markdown('### Variable '+ columnas[v]))
    columns = ['cobertura', 'datos', 'voz', 'otros']
        
    if(v != 'nroaming_traffic' and v != 'consumption_mb'):
        
        sql = f"""
        with a as (
                SELECT
                imsi
                , zeroifnull({variables[v]}) as {v}
                , CASE WHEN ZEROIFNULL({variables[v]}) IS NULL THEN null ELSE ROW_NUMBER() OVER (PARTITION BY imsi ORDER BY ZEROIFNULL({variables[v]})) END AS {columnas[v] + '_count'}
                , CEIL(COUNT(ZEROIFNULL({variables[v]})) OVER (PARTITION BY imsi) / 2) AS {columnas[v] + '_total'}
            FROM {table[v]}
            WHERE
                year between '{first_date.year}' and '{last_date.year}'
                AND month between '{'{:02}'.format(first_date.month)}' and '{'{:02}'.format(last_date.month)}'
                and day between '{'{:02}'.format(first_date.day)}' and '{'{:02}'.format(last_date.day)}'),
            b as (
                select a.{v}, a.imsi
                from a 
                where a.{columnas[v] + '_count'} = a.{columnas[v] + '_total'}),
            c as (
            SELECT 
                DISTINCT imsi, 
                        queja
                    FROM user_qos_daily_complaints_part 
                    WHERE dt_time between '{str(first_date)}' and '{str(last_date)}'
            )
            select b.{v}, zeroifnull(c.queja) as queja, count(*) as total
            from b 
            left join c 
            on c.imsi = b.imsi
        group by {v}, queja"""
        
        sql2 =  f"""with a as (
                SELECT
                imsi
                , zeroifnull({variables[v]}) as {v}
                , CASE WHEN ZEROIFNULL({variables[v]}) IS NULL THEN null ELSE ROW_NUMBER() OVER (PARTITION BY imsi ORDER BY ZEROIFNULL({variables[v]})) END AS {columnas[v] + '_count'}
                , CEIL(COUNT(ZEROIFNULL({variables[v]})) OVER (PARTITION BY imsi) / 2) AS {columnas[v] + '_total'}
            FROM {table[v]}
            WHERE
                year between '{first_date.year}' and '{last_date.year}'
                AND month between '{'{:02}'.format(first_date.month)}' and '{'{:02}'.format(last_date.month)}'
                and day between '{'{:02}'.format(first_date.day)}' and '{'{:02}'.format(last_date.day)}'),
            b as (
                select a.{v}, a.imsi
                from a 
                where a.{columnas[v] + '_count'} = a.{columnas[v] + '_total'}),
            c as (
            SELECT 
                DISTINCT imsi, 
                    cobertura,
                    datos,
                    voz,
                    otros
                    FROM user_qos_daily_complaints_part 
                    WHERE dt_time between '{str(first_date)}' and '{str(last_date)}'
            )
            select b.{v}, zeroifnull(c.cobertura) as cobertura, zeroifnull(c.datos) as datos, zeroifnull(c.voz) as voz, zeroifnull(c.otros) as otros, count(*) as total
            from b 
            left join c 
            on c.imsi = b.imsi
        group by {v}, c.cobertura,c.datos, c.voz, c.otros"""
        
        
      
        
    if(v == 'nroaming_traffic'):
                    
        sql=f"""with t1 as (
            SELECT
            imsi
            ,avg (zeroifnull(nroaming_traffic)) as nroaming_traffic
            FROM {table[v]}
            WHERE
                year between '{first_date.year}' and '{last_date.year}'
                AND month between '{'{:02}'.format(first_date.month)}' and '{'{:02}'.format(last_date.month)}'
                and day between '{'{:02}'.format(first_date.day)}' and '{'{:02}'.format(last_date.day)}'
              group by imsi
              ),
              t2 AS (
                    SELECT t1.*,
                    ROW_NUMBER() OVER(order by t1.{v}) AS contador
                    FROM t1
                    
                    ),
                    num_filas as (
                    select
                        round(count(*)/5, 0)  as p20,
                        round(count(*) * 2/5, 0) as p40,
                        round(count(*) * 3/5, 0) as p60,
                        round(count(*) * 4/5, 0) as p80,
                        round(count(*) , 0) as p100
                        from t2 
                        ),
                    re as (
                    select 	
                    t2.imsi,
                    case 
                        when t2.contador <= num_filas.p20 then 'p20'
                        when t2.contador <= num_filas.p40 and t2.contador > num_filas.p20 then  'p40'
                        when t2.contador <= num_filas.p60 and t2.contador > num_filas.p40 then 'p60'
                        when t2.contador <= num_filas.p80 and t2.contador > num_filas.p60 then  'p80'
                        when t2.contador > num_filas.p80 then  'p100'
                        end as grupo
                        from t2
                    inner join num_filas
                    on num_filas.p20 <= t2.contador
                    or num_filas.p40 <= t2.contador and num_filas.p20 > t2.contador
                    or num_filas.p60 <= t2.contador and num_filas.p40 > t2.contador
                    or num_filas.p80 <= t2.contador and num_filas.p60 > t2.contador
                    or num_filas.p80 > t2.contador
                    ),
                    quejas as (
                    SELECT 
                    distinct imsi, 
                    queja
                    FROM user_qos_daily_complaints_part 
                    WHERE day(dt_time) BETWEEN {first_date.day} and {last_date.day} 
                    AND month(dt_time) between {first_date.month} and {last_date.month}
                    AND year(dt_time) between {first_date.year} and {last_date.year}
                    order by imsi
                    )
                    select re.grupo, quejas.queja, count (*) as total
                    from re
                    left join quejas
                    on quejas.imsi = re.imsi 
                    group by re.grupo, quejas.queja"""
        
        sql2 = f"""with t1 as (
            SELECT
            imsi
            ,avg (zeroifnull(nroaming_traffic)) as nroaming_traffic
            FROM {table[v]}
            WHERE
                year between '{first_date.year}' and '{last_date.year}'
                AND month between '{'{:02}'.format(first_date.month)}' and '{'{:02}'.format(last_date.month)}'
                and day between '{'{:02}'.format(first_date.day)}' and '{'{:02}'.format(last_date.day)}'
              group by imsi
              ), 
                t2 AS (
                    SELECT t1.*,
                    ROW_NUMBER() OVER(order by t1.{v}) AS contador
                    FROM t1
                    
                    ),
                    num_filas as (
                    select
                        round(count(*)/5, 0)  as p20,
                        round(count(*) * 2/5, 0) as p40,
                        round(count(*) * 3/5, 0) as p60,
                        round(count(*) * 4/5, 0) as p80,
                        round(count(*) , 0) as p100
                        from t2 
                        ),
                    re as (
                    select 	
                    t2.imsi,
                    case 
                        when t2.contador <= num_filas.p20 then 'p20'
                        when t2.contador <= num_filas.p40 and t2.contador > num_filas.p20 then  'p40'
                        when t2.contador <= num_filas.p60 and t2.contador > num_filas.p40 then 'p60'
                        when t2.contador <= num_filas.p80 and t2.contador > num_filas.p60 then  'p80'
                        when t2.contador > num_filas.p80 then  'p100'
                        end as grupo
                        from t2
                    inner join num_filas
                    on num_filas.p20 <= t2.contador
                    or num_filas.p40 <= t2.contador and num_filas.p20 > t2.contador
                    or num_filas.p60 <= t2.contador and num_filas.p40 > t2.contador
                    or num_filas.p80 <= t2.contador and num_filas.p60 > t2.contador
                    or num_filas.p80 > t2.contador
                    ),
                    quejas as (
                    SELECT 
                    distinct imsi, 
                    cobertura,
                    datos,
                    voz,
                    otros
                    FROM user_qos_daily_complaints_part 
                    WHERE day(dt_time) BETWEEN {first_date.day} and {last_date.day} 
                    AND month(dt_time) between {first_date.month} and {last_date.month}
                    AND year(dt_time) between {first_date.year} and {last_date.year}
                    order by imsi
                    )
                    select re.grupo, quejas.cobertura,quejas.datos, quejas.voz, quejas.otros, count (*) as total
                    from re
                    left join quejas
                    on quejas.imsi = re.imsi 
                    group by re.grupo, quejas.cobertura,quejas.datos, quejas.voz, quejas.otros"""
        
        quintiles_values = f"""with t1 as (SELECT
                            imsi
                            ,avg (zeroifnull(nroaming_traffic)) as nroaming_traffic
                            FROM user_qos_derived_part
                            WHERE
                                year between '{first_date.year}' and '{last_date.year}'
                                AND month between '{'{:02}'.format(first_date.month)}' and '{'{:02}'.format(last_date.month)}'
                                and day between '{'{:02}'.format(first_date.day)}' and '{'{:02}'.format(last_date.day)}'
                              group by imsi
                              ),
                t2 AS (SELECT t1.*,
                             ROW_NUMBER() OVER(order by t1.nroaming_traffic) AS contador
                             FROM t1
                                    ),
                num_filas as (select
                             count(*)/count(*) as p0,
                             round(count(*)/5, 0)  as p20,
                             round(count(*) * 2/5, 0) as p40,
                             round(count(*) * 3/5, 0) as p60,
                             round(count(*) * 4/5, 0) as p80,
                             round(count(*) , 0) as p100
                                        from t2 
                                        )
                select {v} as {v+'_quintile'} from t2
                inner join num_filas
                on num_filas.p0 = t2.contador
                or num_filas.p20 = t2.contador
                or num_filas.p40 = t2.contador
                or num_filas.p60 = t2.contador
                or num_filas.p80 = t2.contador
                or num_filas.p100 = t2.contador"""

        sql3 = f"""with a as (SELECT
                imsi
                ,round(avg (zeroifnull(nroaming_traffic))) as nroaming_traffic
                FROM user_qos_derived_part
                WHERE
                    year between '{first_date.year}' and '{last_date.year}' 
                    AND month between '{'{:02}'.format(first_date.month)}' and '{'{:02}'.format(last_date.month)}' 
                    and day between '{'{:02}'.format(first_date.day)}' and '{'{:02}'.format(last_date.day)}'
                  group by imsi) 
                select nroaming_traffic, count(nroaming_traffic) as total from a 
                group by nroaming_traffic"""
                
        
        
        
    if(v == 'consumption_mb'):
        
        sql = f"""with t1 as (
                SELECT
                imsi
                , zeroifnull({variables[v]}) as {v}
                , CASE WHEN ZEROIFNULL({variables[v]}) IS NULL THEN null ELSE ROW_NUMBER() OVER (PARTITION BY imsi ORDER BY ZEROIFNULL({variables[v]})) END AS {columnas[v] + '_count'}
                , CEIL(COUNT(ZEROIFNULL({variables[v]})) OVER (PARTITION BY imsi) / 2) AS {columnas[v] + '_total'}
            FROM {table[v]}
            WHERE
                year between '{first_date.year}' and '{last_date.year}'
                AND month between '{'{:02}'.format(first_date.month)}' and '{'{:02}'.format(last_date.month)}'
                and day between '{'{:02}'.format(first_date.day)}' and '{'{:02}'.format(last_date.day)}'),
                     t2 AS (
                    SELECT t1.*,
                    ROW_NUMBER() OVER(order by t1.{v}) AS contador
                    FROM t1
                    WHERE t1.{columnas[v] + '_count'} = t1.{columnas[v] + '_total'}
                    ),
                    num_filas as (
                    select
                        round(count(*)/5, 0)  as p20,
                        round(count(*) * 2/5, 0) as p40,
                        round(count(*) * 3/5, 0) as p60,
                        round(count(*) * 4/5, 0) as p80,
                        round(count(*) , 0) as p100
                        from t2 
                        ),
                    re as (
                    select 	
                    t2.imsi,
                    case 
                        when t2.contador <= num_filas.p20 then 'p20'
                        when t2.contador <= num_filas.p40 and t2.contador > num_filas.p20 then  'p40'
                        when t2.contador <= num_filas.p60 and t2.contador > num_filas.p40 then 'p60'
                        when t2.contador <= num_filas.p80 and t2.contador > num_filas.p60 then  'p80'
                        when t2.contador > num_filas.p80 then  'p100'
                        end as grupo
                        from t2
                    inner join num_filas
                    on num_filas.p20 <= t2.contador
                    or num_filas.p40 <= t2.contador and num_filas.p20 > t2.contador
                    or num_filas.p60 <= t2.contador and num_filas.p40 > t2.contador
                    or num_filas.p80 <= t2.contador and num_filas.p60 > t2.contador
                    or num_filas.p80 > t2.contador
                    ),
                    quejas as (
                    SELECT 
                    distinct imsi, 
                    queja
                    FROM user_qos_daily_complaints_part 
                    WHERE day(dt_time) BETWEEN {first_date.day} and {last_date.day} 
                    AND month(dt_time) between {first_date.month} and {last_date.month}
                    AND year(dt_time) between {first_date.year} and {last_date.year}
                    order by imsi
                    )
                    select re.grupo, quejas.queja, count (*) as total
                    from re
                    left join quejas
                    on quejas.imsi = re.imsi 
                    group by re.grupo, quejas.queja"""
        
                    

    
    
        sql2 = f"""with t1 as (
                SELECT
                imsi
                , zeroifnull({variables[v]}) as {v}
                , CASE WHEN ZEROIFNULL({variables[v]}) IS NULL THEN null ELSE ROW_NUMBER() OVER (PARTITION BY imsi ORDER BY ZEROIFNULL({variables[v]})) END AS {columnas[v] + '_count'}
                , CEIL(COUNT(ZEROIFNULL({variables[v]})) OVER (PARTITION BY imsi) / 2) AS {columnas[v] + '_total'}
            FROM {table[v]}
            WHERE
                year between '{first_date.year}' and '{last_date.year}'
                AND month between '{'{:02}'.format(first_date.month)}' and '{'{:02}'.format(last_date.month)}'
                and day between '{'{:02}'.format(first_date.day)}' and '{'{:02}'.format(last_date.day)}'),
                     t2 AS (
                    SELECT t1.*,
                    ROW_NUMBER() OVER(order by t1.{v}) AS contador
                    FROM t1
                    WHERE t1.{columnas[v] + '_count'} = t1.{columnas[v] + '_total'}
                    ),
                    num_filas as (
                    select
                        round(count(*)/5, 0)  as p20,
                        round(count(*) * 2/5, 0) as p40,
                        round(count(*) * 3/5, 0) as p60,
                        round(count(*) * 4/5, 0) as p80,
                        round(count(*) , 0) as p100
                        from t2 
                        ),
                    re as (
                    select 	
                    t2.imsi,
                    case 
                        when t2.contador <= num_filas.p20 then 'p20'
                        when t2.contador <= num_filas.p40 and t2.contador > num_filas.p20 then  'p40'
                        when t2.contador <= num_filas.p60 and t2.contador > num_filas.p40 then 'p60'
                        when t2.contador <= num_filas.p80 and t2.contador > num_filas.p60 then  'p80'
                        when t2.contador > num_filas.p80 then  'p100'
                        end as grupo
                        from t2
                    inner join num_filas
                    on num_filas.p20 <= t2.contador
                    or num_filas.p40 <= t2.contador and num_filas.p20 > t2.contador
                    or num_filas.p60 <= t2.contador and num_filas.p40 > t2.contador
                    or num_filas.p80 <= t2.contador and num_filas.p60 > t2.contador
                    or num_filas.p80 > t2.contador
                    ),
                    quejas as (
                    SELECT 
                    distinct imsi, 
                    cobertura,
                    datos,
                    voz,
                    otros
                    FROM user_qos_daily_complaints_part 
                    WHERE day(dt_time) BETWEEN {first_date.day} and {last_date.day} 
                    AND month(dt_time) between {first_date.month} and {last_date.month}
                    AND year(dt_time) between {first_date.year} and {last_date.year}
                    order by imsi
                    )
                    select re.grupo, quejas.cobertura, quejas.datos, quejas.voz, quejas.otros, count (*) as total
                    from re
                    left join quejas
                    on quejas.imsi = re.imsi 
                    group by re.grupo, quejas.cobertura,quejas.datos, quejas.voz, quejas.otros"""
        
        quintiles_values = f"""
                    with t1 as (SELECT
                            imsi
                            , zeroifnull({variables[v]}) as {v}
                            , CASE WHEN ZEROIFNULL({variables[v]}) IS NULL THEN null ELSE ROW_NUMBER() OVER (PARTITION BY imsi ORDER BY ZEROIFNULL({variables[v]})) END AS {columnas[v] + '_count'}
                            , CEIL(COUNT(ZEROIFNULL({variables[v]})) OVER (PARTITION BY imsi) / 2) AS {columnas[v] + '_total'}
                        FROM {table[v]}
                        WHERE
                            year between '{first_date.year}' and '{last_date.year}'
                            AND month between '{'{:02}'.format(first_date.month)}' and '{'{:02}'.format(last_date.month)}'
                            and day between '{'{:02}'.format(first_date.day)}' and '{'{:02}'.format(last_date.day)}'
                              ),
                    t2 AS (
                    SELECT t1.*,
                    ROW_NUMBER() OVER(order by t1.{v}) AS contador
                    FROM t1
                    WHERE t1.{columnas[v] + '_count'} = t1.{columnas[v] + '_total'}
                    ),
                num_filas as (select
                             count(*)/count(*) as p0,
                             round(count(*)/5, 0)  as p20,
                             round(count(*) * 2/5, 0) as p40,
                             round(count(*) * 3/5, 0) as p60,
                             round(count(*) * 4/5, 0) as p80,
                             round(count(*) , 0) as p100
                                        from t2 
                                        )
                select {v} as {v + '_quintile'} from t2
                inner join num_filas
                on num_filas.p0 = t2.contador
                or num_filas.p20 = t2.contador
                or num_filas.p40 = t2.contador
                or num_filas.p60 = t2.contador
                or num_filas.p80 = t2.contador
                or num_filas.p100 = t2.contador
                    """
        sql3 = f"""with t1 as ( SELECT
                imsi
                , zeroifnull(consumption_mb) as consumption_mb
                , CASE WHEN ZEROIFNULL(consumption_mb) IS NULL THEN null ELSE ROW_NUMBER() OVER (PARTITION BY imsi ORDER BY ZEROIFNULL(consumption_mb)) END AS consumption_mb_count
                , CEIL(COUNT(ZEROIFNULL(consumption_mb)) OVER (PARTITION BY imsi) / 2) AS consumption_mb_total
            FROM user_qos_derived_part
            WHERE 
                    year between '{first_date.year}' and '{last_date.year}'
                    AND month between '{'{:02}'.format(first_date.month)}' and '{'{:02}'.format(last_date.month)}' 
                    and day between '{'{:02}'.format(first_date.day)}' and '{'{:02}'.format(last_date.day)}'),
                     t2 AS (
                    SELECT t1.*,
                    ROW_NUMBER() OVER(order by t1.consumption_mb) AS contador
                    FROM t1
                    WHERE t1.consumption_mb_count = t1.consumption_mb_total
                    )
                    select round(consumption_mb) as consumption, count(round(consumption_mb)) as total_count from t2
                    group by round(consumption_mb)"""

    impala_int = get_impala_int()
                    
    df = impala_int.select_impala_as_df(sql)
    
    first_column = df.columns[0]

    df = df.fillna(0).groupby([first_column, 'queja']).sum().reset_index()   

    if(v == 'nroaming_traffic' or v == 'consumption_mb'):
        
        df['total_sum'] = df.groupby([first_column])['total'].transform('sum')

        df = df[df['queja'] == 1]

        df['ratio'] = df['total']*10000 /df['total_sum']
        
        #data preparation for ploting CDF ------------------
        
        impala_int = get_impala_int()
        
        a = impala_int.select_impala_as_df(sql3)
        
        if(v == 'consumption_mb'):
            a.columns = ['consumption_mb', 'total']
        
        a = a[a[v]<=threshold*1000]

        
    
    if(v != 'nroaming_traffic' and v != 'consumption_mb'):
        
        # data preparation for ploting CDF -----------------
        a = df.groupby(by = v).sum().reset_index()

        a = a[a[v]<=threshold*10]

   
        
        #----------------------------------------------
        
        complaint_values = list(df[df['queja']==1][first_column])
        df['total_sum'] = df.groupby([first_column])['total'].transform('sum')
        df_q1 = df[df['queja']==1]
        df_q0 = df[df['queja']==0]
        df_q0 = df_q0[df_q0[first_column].isin(complaint_values) == False]

        df_final = df_q1.append(df_q0).sort_values(by = first_column)

        df_final = pd.concat([df_final[df_final[v]<= threshold] , pd.DataFrame(df_final[df_final[v] > threshold].sum()).T]).reset_index(drop = 1)

        df_final['ratio'] = df_final['total']*10000/df_final['total_sum']

        df_q1_f = pd.concat([df_q1[df_q1[v]<= threshold] , pd.DataFrame(df_q1[df_q1[v] > threshold].sum()).T]).reset_index(drop = 1)

        df_q1_f['total_sum'] = df_final['total_sum']

        df_q1_f['ratio'] = df_q1_f['total']*10000/df_q1_f['total_sum']

        df = df_q1_f
        
        #df = pd.concat([df[df[v]<= threshold] , pd.DataFrame(df[df[v] > threshold].sum()).T]).reset_index(drop = 1)
        df[v][df[v]>threshold] = '>'+ str(threshold)
        new_bins = df[v]
    
    # Plot CDF -------------------------------
    
    a = a.sort_values(by='total', ascending = False).reset_index()
    
    if (v == 'nroaming_traffic' or v == 'kqi_16'):
        a = a.sort_values(by=v, ascending = True).reset_index(drop = True)
        
    users = a.iloc[:,-1].sum()
    variable = a.shape[0]
    a['users_cumsum'] = a.iloc[:,-1].cumsum()
    a['p_users_cumsum'] = a['users_cumsum']/users
    a['n'] = a.reset_index().index+1
    a['p_n'] = a['n']/variable

    x = a[v]
    y = a['p_users_cumsum']
    fig, ax = plt.subplots()
    ax.plot(x, y, color = 'r', linewidth = 1)
    ax.set_xlabel('Number of '+ v)
    ax.set_ylabel('Percentage of users')
    plt.title(f'''Distribution percentage users {columnas[v]} per cells''')
    plt.grid()
    plt.show()
    
    # Plot por conjunto global de quejas
    
    height = list(df['total_sum'])
    
    if(v== 'nroaming_traffic' or v == 'consumption_mb'):
        
        impala_int = get_impala_int()
        
        x_labels = impala_int.select_impala_as_df(quintiles_values)
        
        x_labels['percentile'] = [0, 20, 40, 60, 80, 100]

        x_labels[v+'_quintile'] = x_labels[v+'_quintile'].round(2)

        bins = x_labels[v+'_quintile']   
        
        df['grupo'] = [100, 20, 40, 60, 80]

        df = df.sort_values(by='grupo').reset_index()
        
        new_bins = [a for a in zip(bins[:-1],bins[1:])]
        
        # height = list(df['total'])   
    
  
    
    bars = df[first_column]
    y_pos = np.arange(len(bars))
    labels = list(df['total_sum'].astype(int))

    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    ax1.bar(y_pos, height)
    ax2.plot(y_pos, df['ratio'], color = 'r')

    ax1.set_xlabel(f'''{columnas[v]}''')
    ax1.set_ylabel('Number of customers')
    ax2.set_ylabel('Ratio')
    ax2.set_ylim(ymin=0)
    ax1.set_xticklabels(new_bins, rotation = 45)
    ax1.set_xticks(y_pos)
    plt.title(f'''Quejas VS {columnas[v]}''')
    
    rects = ax1.patches
    
    for rect, label in zip(rects, labels):
            height = rect.get_height()
            ax1.text(rect.get_x() + rect.get_width() / 2, height + 5, label,
                    ha='center', va='bottom')

    plt.show()
    
    
    
    impala_int = get_impala_int()
    
    main_df = impala_int.select_impala_as_df(sql2)

    #Plot de las gráficas por tipología de queja
    
    if(v== 'nroaming_traffic' or v == 'consumption_mb'):
        

        impala_int = get_impala_int()

        x_labels = impala_int.select_impala_as_df(quintiles_values)

        x_labels['percentile'] = [0, 20, 40, 60, 80, 100]

        x_labels[v+'_quintile'] = x_labels[v+'_quintile'].round(2)

        bins = x_labels[v+'_quintile']   
    
    for col in columns:
        df = main_df[[first_column, f'{col}', 'total']]
        df = df.fillna(0).groupby([first_column, f'{col}']).sum().reset_index()
        
        if(v !='nroaming_traffic' and v != 'consumption_mb'):
            complaint_values = list(df[df[col]==1][first_column])
            df['total_sum'] = df.groupby([first_column])['total'].transform('sum')
            df_q1 = df[df[col]==1]
            df_q0 = df[df[col]==0]
            df_q0 = df_q0[df_q0[first_column].isin(complaint_values) == False]

            df_final = df_q1.append(df_q0).sort_values(by = first_column)

            df_final = pd.concat([df_final[df_final[v]<= threshold] , pd.DataFrame(df_final[df_final[v] > threshold].sum()).T]).reset_index(drop = 1)

            df_final['ratio'] = df_final['total']*10000/df_final['total_sum']

            df_q1_f = pd.concat([df_q1[df_q1[v]<= threshold] , pd.DataFrame(df_q1[df_q1[v] > threshold].sum()).T]).reset_index(drop = 1)

            df_q1_f['total_sum'] = df_final['total_sum']

            df_q1_f['ratio'] = df_q1_f['total']*10000/df_q1_f['total_sum']

            df = df_q1_f

            df[v][df[v]>threshold] = '>'+ str(threshold)
            new_bins = df[v]
            
        if(v =='nroaming_traffic' or v == 'consumption_mb'):
            
            df['total_sum'] = df.groupby(first_column)['total'].transform('sum')
            df = df[df[f'{col}'] == 1]

            df[f'{col}'] = 1
            df['ratio'] = df['total']*10000/df['total_sum']
            
            df['grupo'] = [100, 20, 40, 60, 80]

            df = df.sort_values(by='grupo').reset_index()

            new_bins = [a for a in zip(bins[:-1],bins[1:])]
        

        
        height = list(df['total_sum'])
    
        
        bars = df[first_column]
        y_pos = np.arange(len(bars))
        labels = list(df['total_sum'].astype(int))

        fig, ax1 = plt.subplots()
        ax2 = ax1.twinx()
        ax1.bar(y_pos, height)
        ax2.plot(y_pos, df['ratio'], color = 'r')

        ax1.set_xlabel(f'''{columnas[v]}''')
        ax1.set_ylabel('Number of customers')
        ax2.set_ylabel('Ratio')
        ax2.set_ylim(ymin=0)
        ax1.set_xticklabels(new_bins, rotation = 45)
        ax1.set_xticks(y_pos)
        plt.title(f'''Quejas tipo {col} VS {columnas[v]}''')
        
        rects = ax1.patches

        for rect, label in zip(rects, labels):
            height = rect.get_height()
            ax1.text(rect.get_x() + rect.get_width() / 2, height + 5, label,
                    ha='center', va='bottom')
        #plt.savefig(f'''{columnas[v]}_vs_quejas_{col}'''+'.png')
        plt.show()   
        



