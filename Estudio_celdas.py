###  CELL BEHAVIOUR ANALYSIS VERSUS DIFFERENT COMPLAINTS TIPOLOGY


# This process creates plots that explain how the customers behave, according to some quality indicators and how do the complaints concentrate in a determined number of cells. The main objective of this script is to verify that 
# a big percentage of the complaints concentrate in a few cells, in order to be able to face them to improve their performance.

# Chunk code to hide code when transforming notebook into HTML file

# %%html
# <script>
#   function code_toggle() {
#     if (code_shown){
#       $('div.input').hide('500');
#       $('#toggleButton').val('Show Code')
#     } else {
#       $('div.input').show('500');
#       $('#toggleButton').val('Hide Code')
#     }
#     code_shown = !code_shown
#   }

#   $( document ).ready(function(){
#     code_shown=false;
#     $('div.input').hide()
#   });
# </script>
# <form action="javascript:code_toggle()"><input type="submit" id="toggleButton" value="Show Code"></form>

%matplotlib inline
import pandas as pd
import numpy as np 
import os
import matplotlib.pyplot as plt
from datetime import datetime, date, timedelta
import logging
import sys
import smtplib
from email.utils import formatdate
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from interfaces.hdfs import HdfsInterface
from interfaces.sftp import SFTPInterface
from interfaces.impala import ImpalaInterface
from interfaces.db import DBInterface
import re
import paramiko 
import unicodedata
import operator
import subprocess
import time
import matplotlib.pyplot as plt; plt.rcdefaults()
import matplotlib.pyplot as plt
import folium
from dateutil.relativedelta import relativedelta, SU
from IPython.display import display, Markdown, Latex
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')

import sys
!conda install --yes --prefix {sys.prefix} plotly

# Standard plotly imports
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import iplot, init_notebook_mode
# Using plotly + cufflinks in offline mode
import cufflinks
cufflinks.go_offline(connected=True)
init_notebook_mode(connected=True)

logger = logging.getLogger(__name__)

#SFTP
SFTP_HOST = '10.100.6.38'
SFTP_PORT = 22
SFTP_USERNAME = 'kernel'
SFTP_PASSWORD = 'Masmovil00*'    
SFTP_ACS_PATH = '/Gestor_ACS/'
SFTP_ACTIVATIONS_PATH = '/DWH/'
SFTP_OUTPUT_PATH = '/Kernel/'

#HDFS
HADOOP_USER_NAME = 'cem'
HADOOP_CONF_DIR = '/churn/code/cem_fija'
HADOOP_HOME = '/churn/hadoop/hadoop-2.6.0'
HADOOP_OPTS = '$HADOOP_OPTS -Djava.library.path=$HADOOP_HOME/lib/'
HADOOP_CONF_PROJECT_DIC = '/churn/code/cem_fija/.hdfscli.cfg'
HDFS_PARENT_FOLDER = f'/user/{HADOOP_USER_NAME}'
HDFS_ACS_ADSL_FOLDER = os.path.join(HDFS_PARENT_FOLDER, 'cem_fija_adsl_quality')
HDFS_ACS_FTTH_FOLDER = os.path.join(HDFS_PARENT_FOLDER, 'cem_fija_ftth_quality')
HDFS_INSTABILITY_FOLDER = os.path.join(HDFS_PARENT_FOLDER, 'cem_fija_instability')

#IMPALA
IMPALA_HOST = "impala.yoigo.inet"
IMPALA_PORT = 21050
IMPALA_DB = "cem"
IMPALA_TIMEOUT = 100
IMPALA_USER = "cem"
IMPALA_PASSWORD = "c3m999"
IMPALA_AUTH_MECHANISM = "PLAIN"

#ORACLE 
ORACLE_USER = 'kernel'
ORACLE_PASSWORD = 'kernel2014'
ORACLE_SID = 'dwyoigo'
ORACLE_HOST = '172.30.180.8'
ORACLE_ENGINE = 'oracle'

def get_impala_int():
    """
    Initialize and return a configured Yoigo Impala interface.
    :return impala_int: ImpalaInterface object
    """
    logger.info("Retrieving custom Yoigo Impala interface...")
    impala_int = ImpalaInterface(user=IMPALA_USER,
                                 password=IMPALA_PASSWORD,
                                 db=IMPALA_DB,
                                 timeout=IMPALA_TIMEOUT)
    return impala_int

def get_oracle_int():
    """
    Initialize and return a configured Yoigo ORACLE interface.
    :return oracle_int: DBInterface object
    """
    logger.info("Retrieving custom Yoigo ORACLE interface...")
    oracle_conn_str = ORACLE_ENGINE + '://' + ORACLE_USER + ':' + ORACLE_PASSWORD + '@' + ORACLE_HOST + '/' + ORACLE_SID
    oracle_int = DBInterface(oracle_conn_str)
    return oracle_int

### ESTUDIO SOBRE LAS CELDAS Y LAS QUEJAS KQI NR CS Handover FR% y sus componentes 

# El objetivo de este análisis ha sido visualizar como se distribuyen las quejas de clientes de MasMovil con respecto al número de celdas donde éstas se cursan, de manera que se puedan encontrar correlaciones entre el comportamiento de la totalidad de usuarios (con respecto a ciertos indicadores detallados a continuación) frente a la generación de quejas. Las variables seleccionadas para este estudio son:

# - Handover attempts
# - Act_pdp_attempts
# - Four_g_attach_attempts
# - nroaming_traffic
# - consumption_mb

# A su vez, el alcance de la horquilla temporal considerada para este estudio es semanal, puesto que éste es el horizonte de tiempo que tiene la reasignación de las celdas más importantes por cliente. 
# Cada uno de los gráficos expuestos, representan cuatro series de datos, en dos grandes familias. Por un lado, el primer conjunto engloba la totalidad de tipologías de quejas, mientras que el segundo, únicamente refleja aquellas quejas derivadas de problemas de cobertura. Al mismo tiempo, cada uno de esos grupos, está compuesto de dos gráficas, relativas a la agrupación de clientes y quejas en las primeras 5000 celdas con mayor volumen. Por otro lado, se ha realizado el mismo tratamiento, pero para las 1000 primeras celdas.

# Cada una de las visualizaciones, contiene cuatro curvas, relativas a los datos siguientes:
# - Total_clients: Totalidad de clientes existentes en el umbral de fechas estudiado
# - Total_complaints: Curva de agregación de la totalidad de las quejas
# - Clients_with_extreme_indicator_value: Conjunto de clientes que tienen un valor de indicador analizado, superior a un límite concreto (prefijado para las variables Handover attempts, act_pdp_attempts y four_g_attach_attempts, y calculado para las variables nroaming_traffic y consumption_mb)
# - Complaints_with_extreme_indicator_value: Representación de la acumulación de quejas cuyos clientes tienen un valor de indicador superior al límite prefijado de antemano.

# Al mismo tiempo, cabe resaltar que, para la variable nroaming_traffic, se agrupan utilizando la media de valores semanales por imsi, mientras que para el resto de las variables, la métrica utilizada para la agrupación es la mediana.

variables = {'kqi_9' : 'zeroifnull(kqi_9) + zeroifnull(kqi_10)',
             'kqi_14' : 'kqi_14',
             'kqi_16' : 'kqi_16',
             'nroaming_traffic' : 'nroaming_traffic',
             'consumption_mb' : 'consumption_mb'}

columnas =   {'kqi_9' : 'ho_attempts',
            'kqi_14' : 'act_pdp_attempts',
            'kqi_16' : 'four_g_attach_attempts',
            'nroaming_traffic' : 'nroaming_traffic',
            'consumption_mb' : 'consumption_mb'           
           }                

map_dict = {'kqi_9' : 'kqi_9 + kqi_10',
            'kqi_14' : 'kqi_14',
            'kqi_16' : 'kqi_16',
            'nroaming_traffic' : 'nroaming_traffic',
            'consumption_mb' : 'consumption_mb'
           }

table = {'kqi_9' : 'user_qos_part_2',
'kqi_14' : 'user_qos_part_2',
'kqi_16' : 'user_qos_part_2',
'nroaming_traffic' : 'user_qos_derived_part',
'consumption_mb' : 'user_qos_derived_part'}


map_dict_2 = {'nroaming_traffic' : 'nroaming_traffic',
            'consumption_mb' : 'consumption_mb'    
}

# threshold stablished for the variables calculated with the median (handover, act_pdp_attempts and four_g_attach_attempts)

threshold = 2


modes = {'kqi_9' : 'voice',
             'kqi_14' : 'voice',
             'kqi_16' : 'voice',
             'nroaming_traffic' : 'gprs_sessions',
             'consumption_mb' : 'gprs_sessions'
    }

var_modes = {'voice' : 'calls',
            'gprs_sessions' : 'sessions'}

# number of cells considered for the analysis
top_cells = [5000, 1000]

complaint_types = ['queja', 'cobertura']

# Se definen las fechas entre las cuales se va a realizar el estudio. El alcance de la horquilla temporal es de una semana. 
# Especial atención  a aquellas semanas que incluyen dos meses diferentes, haría falta una query que recogiera de manera óptima este caso.

# Definition of dates when the analysis will take place (one week long)
# Special attention to those dates including two different months. It would be necessary to create a process able to optimize that case.

last_date = date.today() + relativedelta(weekday=SU(-3)) # Número de semanas atrás hasta el día escogido (SU: sunday)
first_date = last_date - timedelta(days = 6) # Número de días desde el último domingo considerado


# EXTREME_VALUES

# Here we calculate the threshold for the variables nroaming_traffic and consumption_mb using the value that fits de p80 value. This value will be the boundary 
# from which the clients will be considered as extreme ones.
# The threshold of the kqi handover_attempts, act_pdp_attach and four_g_attach_attempts is set in a fixed value: 2

impala_int = get_impala_int()

threshold = 2

# national roaming
sql0=f"""with t1 as (
                SELECT
                imsi
                ,avg (zeroifnull(nroaming_traffic)) as nroaming_traffic
                FROM user_qos_derived_part
                WHERE
                    year between '{first_date.year}' and '{last_date.year}'
                    AND month between '{'{:02}'.format(first_date.month)}' and '{'{:02}'.format(last_date.month)}'
                    and day between '{'{:02}'.format(first_date.day)}' and '{'{:02}'.format(last_date.day)}'
                  group by imsi
                  ), 
                    t2 AS (
                        SELECT t1.*,
                        ROW_NUMBER() OVER(order by t1.nroaming_traffic) AS contador
                        FROM t1

                        ),
                        num_filas as (
                    select
                        round(count(*)*0.8, 0)  as p80
                        from t2 
                        )
                    select  
                    t2.nroaming_traffic as extreme_value
                    from t2
                    inner join num_filas
                    on num_filas.p80 = t2.contador"""

extreme_value_nr = impala_int.select_impala_as_df(sql0)

extreme_value_nr = extreme_value_nr.iloc[0]['extreme_value']

#consumption_mb

sql0 = f"""
                with a as (
                        SELECT
                        imsi
                        , zeroifnull(consumption_mb) as consumption_mb
                        , CASE WHEN ZEROIFNULL(consumption_mb) IS NULL THEN null ELSE ROW_NUMBER() OVER (PARTITION BY imsi ORDER BY ZEROIFNULL(consumption_mb)) END AS 'consumption_mb_count'
                        , CEIL(COUNT(ZEROIFNULL(consumption_mb)) OVER (PARTITION BY imsi) / 2) AS 'consumption_mb_total'
                    FROM user_qos_derived_part
                    WHERE
                        year between '{first_date.year}' and '{last_date.year}'
                        AND month between '{'{:02}'.format(first_date.month)}' and '{'{:02}'.format(last_date.month)}'
                        and day between '{'{:02}'.format(first_date.day)}' and '{'{:02}'.format(last_date.day)}'),
                a1 as (
                        select a.consumption_mb, a.imsi
                        from a 
                        where a.consumption_mb_count = a.consumption_mb_total),
                  t2 AS (
                        SELECT a1.*,
                        ROW_NUMBER() OVER(order by a1.consumption_mb) AS contador
                        FROM a1                    
                        ),
                        num_filas as (
                    select
                        round(count(*)*0.80, 0)  as p80
                        from t2 
                        )
                    select  
                    t2.consumption_mb as extreme_value
                    from t2
                    inner join num_filas
                    on num_filas.p80 = t2.contador"""

extreme_value = impala_int.select_impala_as_df(sql0)
extreme_value = extreme_value.iloc[0]['extreme_value']

limit ={'kqi_9' : threshold,
             'kqi_14' : threshold,
             'kqi_16' : threshold,
             'nroaming_traffic' : extreme_value_nr,
             'consumption_mb' : extreme_value}


# Para cada una de las variables consideradas, procesamos los datos para las 5000 y 1000 celdas con mayor concentración de volumen de clientes del indicador considerado
# Todas las variables se procesan a nivel de imsi, descartando aquellos clientes que no son de masmovil, ya que la relación de la tabla imsi_to_ms_seq no existe para aquellos clientes
# que no son de la compañía sino que utilizan su red (por ejemplo, clientes de pepephone)

# The data is obtained from cem table. The processsus to link the tables and generate the output is the following:
# 1. Depending on the indicator, whe get the data from user_qos_part_2 table or user_qos_derived_part between the considered dates, calculating the median or the mean value by imsi
# 2. Data import from imsi_to_ms_seq table
# 3. Join 2. and 1.
# 4. Data import from user_complaints_daily_part
# 5. Join 3. and 4.
# 6. Data import from qos.qoe_top_cells_XXX_(voice or gprs_sessions)weighted
# 7. Data import from qoe_top_cells_geo
# 8. Join 5., 6. and 7. and calculation of specfic parameters (complaints with high indicator value, clients with high indicator value)

for v in variables:
    display(Markdown('### Variable '+ columnas[v]))
    
    for n in top_cells:
        for type in complaint_types:


            if (v != 'nroaming_traffic'):
                sql = f"""
                        with a as (
                                SELECT
                                imsi
                                , zeroifnull({variables[v]}) as {v}
                                , CASE WHEN ZEROIFNULL({variables[v]}) IS NULL THEN null ELSE ROW_NUMBER() OVER (PARTITION BY imsi ORDER BY ZEROIFNULL({variables[v]})) END AS {columnas[v] + '_count'}
                                , CEIL(COUNT(ZEROIFNULL({variables[v]})) OVER (PARTITION BY imsi) / 2) AS {columnas[v] + '_total'}
                            FROM {table[v]}
                            WHERE
                                year between '{first_date.year}' and '{last_date.year}'
                                AND month between '{'{:02}'.format(first_date.month)}' and '{'{:02}'.format(last_date.month)}'
                                and day between '{'{:02}'.format(first_date.day)}' and '{'{:02}'.format(last_date.day)}'),
                            a1 as (
                                select a.{v}, a.imsi
                                from a 
                                where a.{columnas[v] + '_count'} = a.{columnas[v] + '_total'}),
                            b as (
                                SELECT *
                                FROM imsi_to_ms_seq),
                            c as (
                                SELECT a1.*, b.ms_seq
                                from a1
                                left join b 
                                on b.imsi = a1.imsi
                                ),  
                            q as (
                                SELECT 
                                imsi, 
                                ({type}) as queja
                                FROM user_qos_daily_complaints_part 
                                WHERE dt_time between '{str(first_date)}' and '{str(last_date)}'
                            ),
                            t as (
                                SELECT c.{v},c.imsi, 
                                CASE WHEN c.ms_seq is null THEN 'NULO' ELSE c.ms_seq END AS ms_seq,
                                zeroifnull(q.queja) AS queja
                                FROM c
                                LEFT join q 
                                ON q.imsi = c.imsi ),
                            p1 as (
                                SELECT t.*, e.locationinformation_hex, e.{var_modes[modes[v]]}_percent from t
                                INNER JOIN qos.top_cells_{modes[v]}_weighted e 
                                ON e.ms_seq = t.ms_seq
                                WHERE e.val_start = '{first_date.strftime('%Y%m%d')}'),
                            p2 as (
                                SELECT p1.*, f.npro, f.lat, f.lon from p1
                                LEFT JOIN qos.qoe_cells_geo f
                                ON f.locationinformation_hex = p1.locationinformation_hex
                                ),
                            r1 as (
                               SELECT p2.*, 
                                CASE WHEN p2.{v} > {limit[v]} THEN 1 ELSE 0 END AS high_var,
                                queja * p2.{var_modes[modes[v]]}_percent AS queja_sessions 
                                FROM p2  
                                ),
                            r2 as (
                                SELECT r1.*, 
                                high_var * {var_modes[modes[v]]}_percent AS hv,
                                queja_sessions * high_var AS hq 
                                FROM r1),
                            r3 as (
                            SELECT locationinformation_hex, npro, lat, lon,
                            sum({var_modes[modes[v]]}_percent) AS total_clients,
                            CASE WHEN sum(queja)>0 THEN 1 ELSE 0 END AS quejas_global,
                            CASE WHEN sum(high_var)>0 THEN 1 ELSE 0 END AS high_value_global,
                            sum(queja_sessions) AS quejas_total,
                            sum(hv) AS clients_with_high_indicator,
                            sum(hq) AS quejas_with_high_indicator
                            FROM r2
                            GROUP BY locationinformation_hex,  npro, lat, lon
                            )
                            SELECT * FROM r3
                            """


            if(v == 'nroaming_traffic'):


                sql = f"""
                        with a as (
                                SELECT
                                imsi
                                ,avg (zeroifnull(nroaming_traffic)) as nroaming_traffic
                            FROM {table[v]}
                            WHERE
                                year between '{first_date.year}' and '{last_date.year}'
                                AND month between '{'{:02}'.format(first_date.month)}' and '{'{:02}'.format(last_date.month)}'
                                and day between '{'{:02}'.format(first_date.day)}' and '{'{:02}'.format(last_date.day)}'
                                group by imsi),
                            b as (
                                SELECT *
                                FROM imsi_to_ms_seq),
                            c as (
                                SELECT a.*, b.ms_seq
                                from a
                                left join b 
                                on b.imsi = a.imsi
                                ),  
                            q as (
                                SELECT 
                                imsi, 
                                ({type}) as queja
                                FROM user_qos_daily_complaints_part 
                                WHERE dt_time between '{str(first_date)}' and '{str(last_date)}'
                                ),
                            t as (
                                SELECT c.{v},c.imsi, 
                                CASE WHEN c.ms_seq is null THEN 'NULO' ELSE c.ms_seq END AS ms_seq,
                                zeroifnull(q.queja) AS queja
                                FROM c
                                LEFT join q 
                                ON q.imsi = c.imsi ),
                            p1 as (
                                SELECT t.*, e.locationinformation_hex, e.sessions_percent from t
                                INNER JOIN qos.top_cells_{modes[v]}_weighted e 
                                ON e.ms_seq = t.ms_seq
                                WHERE e.val_start = '{first_date.strftime('%Y%m%d')}'),
                            p2 as (
                                SELECT p1.*, f.npro, f.lat, f.lon from p1
                                LEFT JOIN qos.qoe_cells_geo f
                                ON f.locationinformation_hex = p1.locationinformation_hex
                                ),
                            r1 as (
                               SELECT p2.*, 
                                CASE WHEN p2.{v} > {limit[v]} THEN 1 ELSE 0 END AS high_var,
                                queja * p2.{var_modes[modes[v]]}_percent AS queja_sessions 
                                FROM p2  
                                ),
                            r2 as (
                                SELECT r1.*, 
                                high_var * sessions_percent AS hv,
                                queja_sessions * high_var AS hq 
                                FROM r1),
                            r3 as (
                            SELECT locationinformation_hex, npro, lat, lon,
                            sum({var_modes[modes[v]]}_percent) AS total_clients,
                            CASE WHEN sum(queja)>0 THEN 1 ELSE 0 END AS quejas_global,
                            CASE WHEN sum(high_var)>0 THEN 1 ELSE 0 END AS high_value_global,
                            sum(queja_sessions) AS quejas_total,
                            sum(hv) AS clients_with_high_indicator,
                            sum(hq) AS quejas_with_high_indicator
                            FROM r2
                            GROUP BY locationinformation_hex,  npro, lat, lon
                            )
                            SELECT * FROM r3
                            """

          
            impala_int = get_impala_int()

            cells = impala_int.select_impala_as_df(sql)


# We treat every dataframe independently so we have as many objects, as variables we want to plot in the same visualization tool.


            cells_location_df = cells[['locationinformation_hex', 'npro', 'lat', 'lon']]
            cells_df = cells.groupby(by = 'locationinformation_hex').sum()
            cells_df = cells_df.sort_values(by = 'clients_with_high_indicator', ascending=False).reset_index()

            #cells_df = cells_df[:5000]

            cells_df1 = cells_df[['locationinformation_hex', 'total_clients']]
            cells_df2 = cells_df[['locationinformation_hex', 'quejas_total']]
            cells_df3 = cells_df[['locationinformation_hex', 'clients_with_high_indicator']]
            cells_df4 = cells_df[['locationinformation_hex', 'quejas_with_high_indicator']]

            n_cells = cells_location_df.shape[0]

            bad_users_df1 = cells_df1.iloc[:,-1].sum()
            bad_users_df2 = cells_df2.iloc[:,-1].sum()
            bad_users_df3 = cells_df3.iloc[:,-1].sum()
            bad_users_df4 = cells_df4.iloc[:,-1].sum()

            cells_df1['bad_users_cumsum'] = cells_df1.iloc[:,-1].cumsum()
            cells_df1['p_bad_users_cumsum'] = cells_df1['bad_users_cumsum']/bad_users_df1
            cells_df1['n_cells'] = cells_df1.reset_index().index+1
            cells_df1['p_n_cells'] = cells_df1['n_cells']/n_cells
            cells_df1 = cells_df1[:n]

            cells_df2['bad_users_cumsum'] = cells_df2.iloc[:,-1].cumsum()
            cells_df2['p_bad_users_cumsum'] = cells_df2['bad_users_cumsum']/bad_users_df2
            cells_df2['n_cells'] = cells_df2.reset_index().index+1
            cells_df2['p_n_cells'] = cells_df2['n_cells']/n_cells
            cells_df2 = cells_df2[:n]

            cells_df3['bad_users_cumsum'] = cells_df3.iloc[:,-1].cumsum()
            cells_df3['p_bad_users_cumsum'] = cells_df3['bad_users_cumsum']/bad_users_df3
            cells_df3['n_cells'] = cells_df3.reset_index().index+1
            cells_df3['p_n_cells'] = cells_df3['n_cells']/n_cells
            cells_df3 = cells_df3[:n]

            cells_df4['bad_users_cumsum'] = cells_df4.iloc[:,-1].cumsum()
            cells_df4['p_bad_users_cumsum'] = cells_df4['bad_users_cumsum']/bad_users_df4
            cells_df4['n_cells'] = cells_df4.reset_index().index+1
            cells_df4['p_n_cells'] = cells_df4['n_cells']/n_cells
            cells_df4 = cells_df4[:n]
            
            if type == 'queja':
            
                plt.plot('n_cells', 'p_bad_users_cumsum', data = cells_df1, color = 'r', label = 'total_clients')
                plt.plot('n_cells', 'p_bad_users_cumsum', data = cells_df2, color = 'b', label = 'total_complaints')
                plt.plot('n_cells', 'p_bad_users_cumsum', data = cells_df3, color = 'g', label = 'clients_with_extreme_indicator_value')
                plt.plot('n_cells', 'p_bad_users_cumsum', data = cells_df4, color = 'darkorange', label = 'complaints_with_extreme_indicator_value')
                plt.title(f'''Distribution percentage users {columnas[v]} per cells (Top {n})''')
            else:
                plt.plot('n_cells', 'p_bad_users_cumsum', data = cells_df1, color = 'fuchsia', label = 'total_clients')
                plt.plot('n_cells', 'p_bad_users_cumsum', data = cells_df2, color = 'deepskyblue', label = 'total_coverage_complaints')
                plt.plot('n_cells', 'p_bad_users_cumsum', data = cells_df3, color = 'lime', label = 'clients_with_extreme_indicator_value')
                plt.plot('n_cells', 'p_bad_users_cumsum', data = cells_df4, color = 'gold', label = 'coverage_complaints_with_extreme_indicator_value')
                plt.title(f'''Distribution percentage users {columnas[v]} per cells (Top {n}_coverage complaints)''')
            plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), shadow=True, ncol=4)
            plt.show()




